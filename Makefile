export APP = merge-request-bot
export TAG = $(shell ./hack/tag.sh)
export IMAGE = gcr.io/$(GCP_PROJECT_ID)/$(APP):$(TAG)

export CGO_ENABLED = 0

.PHONY: tests
tests:
	go test -v -coverprofile=cover.out ./...
	go tool cover -func cover.out

.PHONY: lint
lint: OUT_FORMAT ?= line-number
lint: SHELL=/bin/bash
lint:
	set -o pipefail; golangci-lint run --out-format $(OUT_FORMAT) | tee gl-code-quality-report.json

.PHONY: build
build:
	go build -o function cmd/function/main.go

.PHONY: gcp-build
gcp-build:
	gcloud builds submit --gcs-log-dir=$(GCP_CLOUD_BUILD_LOGS_DIR) --tag $(IMAGE)

.PHONY: cloud-run-deploy
cloud-run-deploy:
	gcloud run deploy $(APP) --image $(IMAGE) --platform managed --region $(CLOUD_RUN_REGION)
