#!/usr/bin/env sh

TAG="dev"

# If running script on master branch in GitLab CI
if [ "$CI_COMMIT_BRANCH" = "master" ]; then
    TAG="latest"
fi

# If running script on a development branch in GitLab CI
if [ ! -z ${CI_MERGE_REQUEST_IID} ]; then
    TAG="mr-${CI_MERGE_REQUEST_IID}"
fi

echo $TAG
