package gitlab

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/machinebox/graphql"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestNewClient(t *testing.T) {
	const token = "TOKEN"

	const expectedBody = `{"query":"{ currentUser { username } }","variables":null}
`

	logger := logrus.New()
	logger.SetLevel(logrus.DebugLevel)

	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer r.Body.Close()
		b, err := ioutil.ReadAll(r.Body)
		require.NoError(t, err)
		assert.Equal(t, expectedBody, string(b))

		w.Header().Set("Content-Type", "application/json")
		_, err = fmt.Fprint(w, `{ "data": { "currentUser": { "username": "username" } } }`)
		require.NoError(t, err)
	}))

	c := NewClient(srv.URL, token, logger)

	req := graphql.NewRequest("{ currentUser { username } }")
	resp := struct {
		CurrentUser struct {
			Username string `json:"username"`
		} `json:"currentUser"`
	}{}

	err := c.graphql.Run(context.Background(), req, &resp)
	require.NoError(t, err)
	assert.Equal(t, resp.CurrentUser.Username, "username")
}

func TestNewClient_NullLogger(t *testing.T) {
	c := NewClient("https://gitlab.com/api/graphql", "token", nil)
	assert.NotNil(t, c.logger)
}

func TestClient_CreateMergeRequestNote(t *testing.T) {
	const token = "TOKEN"

	const id = 1234

	req := struct {
		Variable struct {
			Body string `json:"body"`
			ID   string `json:"id"`
		} `json:"variables"`
	}{}

	tests := []struct {
		resp    string
		wantErr bool
	}{
		{
			resp:    `{ "data": { "createNote": { "note": {  } } } }`,
			wantErr: false,
		},
		{
			resp:    "",
			wantErr: true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.resp, func(t *testing.T) {
			srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				defer r.Body.Close()
				b, err := ioutil.ReadAll(r.Body)
				require.NoError(t, err)

				err = json.Unmarshal(b, &req)
				require.NoError(t, err)

				assert.Equal(t, noteBody, req.Variable.Body)
				assert.Equal(t, fmt.Sprintf("gid://gitlab/MergeRequest/%d", id), req.Variable.ID)

				w.Header().Set("Content-Type", "application/json")
				_, err = fmt.Fprint(w, tt.resp)
				require.NoError(t, err)
			}))

			c := NewClient(srv.URL, token, nil)

			err := c.CreateMergeRequestNote(context.Background(), id)
			if tt.wantErr {
				assert.Error(t, err)
				return
			}
			assert.NoError(t, err)
		})
	}
}
