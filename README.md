# Merge Request Bot

Post a comment as soon as a merge request is opened by a community member that
is not a GitLab team member indicating what are the next steps.

![merge request bot demo](https://i.imgur.com/DjQ3oWs.png)

## Deploy

### [GitLab Serverless](https://docs.gitlab.com/ee/user/project/clusters/serverless/)

To deploy this we use [GitLab Serverless](https://docs.gitlab.com/ee/user/project/clusters/serverless) integration:

1. [Set up Knative+Kubernetes integration with project](https://docs.gitlab.com/ee/user/project/clusters/serverless/#installing-knative-via-gitlabs-kubernetes-integration)
1. [Run pipeline on master to deploy function](https://gitlab.com/steveazz/merge-request-bot/pipelines/new)
    1. The following environment variables should be configured:
        - `GITLAB_GRAPHQL`: The GraphQL endpoint of the GitLab instance for
        example `https://gitlab.com/api/graphql`
        - `GITLAB_PAT`: The [personal access
        token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html)
        that is going to be used to post the comment. The `api` scope is
        required.
        - `GITLAB_WEBHOOK_SECRET_TOKEN`: A secret token to be used to [webhook
        secret](https://docs.gitlab.com/ee/user/project/integrations/webhooks.html#secret-token).
        You should generate a random string, which will be used later for
        [configuredtion](#configure)
1. [Function is deployed to knative](https://gitlab.com/steveazz/merge-request-bot/-/serverless/functions).

### [Cloud Run](https://cloud.google.com/run/)

You might not want to deploy to GitLab serverless because having a
Knative cluster just for 1 function is expensive.

This project automatically deploys to a cloud run service through CI.
The service is publicly available so anything can send requests to it.
The service is defined in
[infrastructure/cloud-run](https://gitlab.com/merge-request-bot/infrastructure/cloud-run).

For a succesful deployment the project requires the following
[variables](https://docs.gitlab.com/ee/ci/variables)
- `CLOUD_RUN_REGION`: The region to deploy too for example
  `europe-west4`
- `GCP_CLOUD_BUILD_LOGS_DIR`: The path of the GCS bucket for [cloud
  build](https://cloud.google.com/cloud-build/) logs, for example
  `gs://merge-request-bot-cloud-build-3104b86/logs`
- `GCP_PROJECT_ID`: The ID of the project where the cloud run service is
  created in.
- `GCP_SERVICE_ACCOUNT`: A [file
  based](https://docs.gitlab.com/ee/ci/variables/#custom-environment-variables-of-type-file)
  variable which is the [service
  account](https://cloud.google.com/compute/docs/access/service-accounts)
  created by the [infrastructure/cloud-run](https://gitlab.com/merge-request-bot/infrastructure/cloud-run).

## Configure

This should be configured as a [GitLab
webhook](https://docs.gitlab.com/ee/user/project/integrations/webhooks.html)
for the [merge request event](https://docs.gitlab.com/ee/user/project/integrations/webhooks.html#merge-request-events).
This webhook listens for all the merge request events but only post
comments when a new merge request by a community member is created.

![webhook configuration example](https://i.imgur.com/6Ly3Uqx.png)

### [GitLab Serverless](#gitlab-serverless)

1. Set `URL` to be the value of the [knative
  deployment](https://gitlab.com/steveazz/merge-request-bot/-/serverless/functions).
1. Set the `Secret Token` to be the same value as
  `GITLAB_WEBHOOK_SECRET_TOKEN` set when [deploying](#deploy) the
  function.
1. Tick `Merge request events`

### [Cloud Run](#cloud-run)

1. Set `URL` to the value of the cloud run service shown in GCP console.
1. Set the `Secret Token` to be the same value as
`GITLAB_WEBHOOK_SECRET_TOKEN` set in
[infrastructure/cloud-run](https://gitlab.com/merge-request-bot/infrastructure/cloud-run).
1. Tick `Merge request events`.
