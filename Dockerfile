FROM golang:1.14.1-alpine3.11 AS builder

WORKDIR /workspace

COPY . .

RUN apk add --no-cache make bash

RUN make build

FROM alpine:3.11

COPY --from=builder /workspace/function /function/merge-request-bot

ENV PORT 8080
EXPOSE 8080

CMD ["/function/merge-request-bot"]
